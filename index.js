const request = require('request')
const nodemailer = require('nodemailer')
const config = require('./config.json')

const url = config.url // get URL to check from config
let oldBody = '' // init string to store old body

console.log('Initializing...')

// request page for first time to have something to compare
requestPage(url)
    .then(body => oldBody = body)
    .catch(err => console.error(err))

// set interval to check website periodically
setInterval(() => {
    checkForChanges(url)
}, 1000 * 30)

// request a page and if successfull resolve stringified body
function requestPage(url) {
    return new Promise((resolve, reject) => {
        request(url, (error, response, body) => {

            if (response && response.statusCode === 200) {
                resolve(JSON.stringify(body))
            } else reject(error)
        })
    })
}

// check if changes happended, log and send mail if change occurred
function checkForChanges() {
    requestPage(url)
        .then(newBody => {

            let currDate = `[${new Date().toLocaleString()}] `

            if (oldBody !== newBody) {
                console.log(currDate + 'change!')
                sendMail()
            } else console.log(currDate + 'check')

            oldBody = newBody
        })
        .catch(err => console.error(err))
}

async function sendMail() {
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: config.host,
        port: config.port,
        secure: config.secure, // true for 465, false for other ports
        auth: {
            user: config.auth.user, // generated ethereal user
            pass: config.auth.pass // generated ethereal password
        }
    });

    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: `${config.from_name} <${config.from_mail}>`, // sender address
        to: config.to, // list of receivers
        subject: config.subject, // Subject line
        text: `change on ${url}` // plain text body
        //html: "<b>Hello world?</b>" // html body
    });

    console.log("Message sent!");
}